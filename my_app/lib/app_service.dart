import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'data.dart';
import 'main.dart';

var lastId = 3;
var mockDatas = [
  Data(
      id: 1,
      title: 'Present',
      descript: 'Show Descript 1',
      time: DateTime.now()),
  Data(
      id: 2,
      title: 'ShowNotic',
      descript: 'Show Descript 2',
      time: DateTime.now())
];

int getNewId() {
  return lastId++;
}

Future<void> addNewData(Data data) {
  return Future.delayed(Duration(seconds: 1), () {
    mockDatas.add(Data(
        id: getNewId(),
        title: data.title,
        descript: data.descript,
        time: data.time));
    _showNotification(data);
  });
}

Future<void> saveData(Data data) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDatas.indexWhere((item) => item.id == data.id);
    mockDatas[index] = data;
    _showNotification(data);
  });
}

Future<void> delData(Data data) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDatas.indexWhere((item) => item.id == data.id);
    mockDatas.removeAt(index);
  });
}

Future<List<Data>> getData() {
  return Future.delayed(Duration(seconds: 1), () => mockDatas);
}

Future<void> _showNotification(Data data) async {
  const AndroidNotificationDetails androidNotificationDetails =
      AndroidNotificationDetails('nextflow_noti_001', 'แจ้งเตือนทั่วไป',
          importance: Importance.max,
          priority: Priority.high,
          ticker: 'ticker');

  const NotificationDetails platformChannelDetails = NotificationDetails(
    android: androidNotificationDetails,
  );

  var scheduledTime = data.time;
  flutterLocalNotificationsPlugin.schedule(
      1, data.title, data.descript, scheduledTime, platformChannelDetails);

  // await flutterLocalNotificationsPlugin.show(
  //     0, data.title, data.descript, platformChannelDetails);
}
