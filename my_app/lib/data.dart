class Data {
  int id;
  String title;
  String descript;
  var time = DateTime.now();

  Data(
      {required this.id,
      required this.title,
      required this.descript,
      required this.time});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'descript': descript,
      'time': time,
    };
  }

  static List<Data> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Data(
        id: maps[i]['id'],
        title: maps[i]['title'],
        descript: maps[i]['descript'],
        time: maps[i]['time'],
      );
    });
  }

  @override
  String toString() {
    return 'Dog {id: $id,title: $title, descript: $descript, time: $time}';
  }
}
