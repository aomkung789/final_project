import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'data.dart';
import 'app_service.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class DataForm extends StatefulWidget {
  Data data;
  DataForm({Key? key, required this.data}) : super(key: key);

  @override
  _DataFormState createState() => _DataFormState(data);
}

class _DataFormState extends State<DataForm> {
  final _formKey = GlobalKey<FormState>();
  Data data;
  _DataFormState(this.data);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Form')),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                TextFormField(
                  autofocus: true,
                  initialValue: data.title,
                  decoration: InputDecoration(labelText: 'หัวข้อ'),
                  onChanged: (String? value) {
                    data.title = value!;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'โปรดใส่หัวข้อ';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  initialValue: data.descript,
                  decoration:
                      InputDecoration(labelText: 'รายละเอียดการแจ้งเตือน'),
                  onChanged: (String? value) {
                    data.descript = value!;
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'โปรดใส่รายละเอียดการแจ้งเตือน';
                    }
                    return null;
                  },
                ),
                DateTimeField(
                  // controller: _timeController,
                  initialValue: data.time,
                  decoration: InputDecoration(labelText: 'เวลาแจ้งเตือน'),
                  format: DateFormat("yyyy-MM-dd HH:mm"),
                  onShowPicker: (context, currentValue) async {
                    final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime.now(),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                    if (date != null) {
                      final time = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.fromDateTime(
                            currentValue ?? DateTime.now()),
                      );
                      return DateTimeField.combine(date, time);
                    } else {
                      return currentValue;
                    }
                  },
                  onChanged: (value) {
                    setState(() {
                      data.time = value!;
                    });
                  },
                  validator: (value) {
                    if (value == null) {
                      return 'กรุใส่เวลาแจ้งเตือน';
                    }
                    return null;
                  },
                ),
                ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        if (data.id > 0) {
                          await saveData(data);
                        } else {
                          await addNewData(data);
                        }
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Done'))
              ],
            ),
          ),
        ));
  }
}
