import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget showLogo() {
  return Container(
    padding: EdgeInsets.all(8),
    width: 100,
    height: 100,
    child: Image.asset('images/logo.png'),
  );
}

Widget showList1() {
  final Map<String, WidgetBuilder>? routes;
  return ListTile(
    leading: Icon(Icons.list_alt),
    title: Text('List'),
    onTap: () {
      // Navigator()
    },
  );
}

Widget showList2() {
  final Map<String, WidgetBuilder>? routes;
  return ListTile(
    leading: Icon(Icons.date_range_outlined),
    title: Text('Schedul'),
    onTap: () {
      // Navigator()
    },
  );
}

Widget showAppName() {
  return Text(
    'Reminder',
    style: TextStyle(
        color: Colors.blue[50], fontWeight: FontWeight.bold, fontSize: 18),
  );
}

Widget showHead() {
  return DrawerHeader(
    decoration: BoxDecoration(color: Colors.indigo[200]),
    child: Column(
      children: [showLogo(), showAppName()],
    ),
  );
}

Widget showDrawer() {
  return Drawer(
    child: ListView(
      children: [showHead(), showList1(), showList2()],
    ),
  );
}
