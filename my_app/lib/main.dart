import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'reminder_widget.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
void main() {
  runApp(MaterialApp(
    theme: ThemeData(
        brightness: Brightness.dark, primaryColor: Colors.indigo[600]),
    home: MyApp(),
  ));
}

// Future<void> main() async {
//   WidgetsFlutterBinding.ensureInitialized();

//   const AndroidInitializationSettings initializationSettingsAndroid =
//       AndroidInitializationSettings("@mipmap/ic_launcher");

//   final InitializationSettings initializationSettings =
//       InitializationSettings(android: initializationSettingsAndroid);

//   await flutterLocalNotificationsPlugin.initialize(initializationSettings);
//   runApp(MaterialApp(
//       title: 'Reminder',
//       home: MyApp(),
//       theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFFEFEFEF))));
// }
