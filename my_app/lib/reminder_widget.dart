import 'package:flutter/material.dart';
import 'package:my_app/app_service.dart';
import 'package:intl/intl.dart';
import 'package:my_app/drawer.dart';
import 'package:sqflite/sqflite.dart';
import 'data.dart';
import 'data_form.dart';

class MyApp extends StatefulWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Future<List<Data>> _data;
  DateTime now = DateTime.now();

  @override
  void initState() {
    super.initState();
    _data = getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Reminder'),
      ),
      body: FutureBuilder(
          future: _data,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Text('Error');
            }
            List<Data> datas = snapshot.data as List<Data>;
            return ListView.builder(
              itemBuilder: (context, index) {
                var data = datas.elementAt(index);
                String formatDate =
                    DateFormat('yyyy-MM-dd – kk:mm').format(data.time);
                return ListTile(
                  leading: const Icon(
                    Icons.timer_outlined,
                    size: 42,
                  ),
                  title: Text(data.title),
                  subtitle: Text('เวลาแจ้งเตือน : ${formatDate}'),
                  trailing: IconButton(
                    icon: const Icon(
                      Icons.delete_forever_outlined,
                      size: 38,
                    ),
                    onPressed: () async {
                      await delData(data);
                      setState(() {
                        _data = getData();
                      });
                    },
                  ),
                  onTap: () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DataForm(data: data)));
                    setState(() {
                      _data = getData();
                    });
                  },
                );
              },
              itemCount: datas.length,
            );
          }),
      drawer: showDrawer(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          Data newData =
              Data(id: -1, title: '', descript: '', time: DateTime.now());
          await Navigator.push(context,
              MaterialPageRoute(builder: (context) => DataForm(data: newData)));
          setState(() {
            _data = getData();
          });
        },
      ),
    );
  }
}
